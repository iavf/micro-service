package com.module.admin.prj.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.prj.dao.PrjOptimizeLogDao;
import com.module.admin.prj.pojo.PrjOptimizeLog;
import com.module.admin.prj.service.PrjInfoService;
import com.module.admin.prj.service.PrjOptimizeLogService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * prj_optimize_log的Service
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
@Component
public class PrjOptimizeLogServiceImpl implements PrjOptimizeLogService {

	@Autowired
	private PrjOptimizeLogDao prjOptimizeLogDao;
	@Autowired
	private PrjInfoService prjInfoService;
	
	@Override
	public void save(PrjOptimizeLog prjOptimizeLog) {
		prjOptimizeLogDao.save(prjOptimizeLog);
	}

	@Override
	public PrjOptimizeLog get(Integer id) {
		return prjOptimizeLogDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(PrjOptimizeLog prjOptimizeLog) {
		prjOptimizeLog.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = prjOptimizeLogDao.findPrjOptimizeLogCount(prjOptimizeLog);
		List<PrjOptimizeLog> data = null;
		if(total > 0) {
			data = prjOptimizeLogDao.findPrjOptimizeLog(prjOptimizeLog);
			for (PrjOptimizeLog pol : data) {
				pol.setPrjName(prjInfoService.getName(pol.getPrjId()));
			}
		}
		Page<PrjOptimizeLog> page = new Page<PrjOptimizeLog>(prjOptimizeLog.getPage(), prjOptimizeLog.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(Integer id) {
		ResponseFrame frame = new ResponseFrame();
		prjOptimizeLogDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public PrjOptimizeLog getDtl(Integer id) {
		PrjOptimizeLog log = get(id);
		if(log != null) {
			log.setPrjName(prjInfoService.getName(log.getPrjId()));
		}
		return log;
	}

	@Override
	public ResponseFrame deleteAll() {
		ResponseFrame frame = new ResponseFrame();
		prjOptimizeLogDao.deleteAll();
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
}
package com.module.admin.prj.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.module.admin.prj.dao.PrjApiTestDao;
import com.module.admin.prj.enums.PrjApiTestDtlStatus;
import com.module.admin.prj.pojo.PrjApiTest;
import com.module.admin.prj.pojo.PrjApiTestDtl;
import com.module.admin.prj.pojo.PrjMonitor;
import com.module.admin.prj.service.PrjApiTestDtlService;
import com.module.admin.prj.service.PrjApiTestService;
import com.module.admin.prj.service.PrjMonitorService;
import com.system.auth.AuthUtil;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameHttpUtil;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameMapUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * prj_api_test的Service
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
@Component
public class PrjApiTestServiceImpl implements PrjApiTestService {

	@Autowired
	private PrjApiTestDao prjApiTestDao;
	@Autowired
	private PrjApiTestDtlService prjApiTestDtlService;
	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	@Autowired
	private PrjMonitorService prjMonitorService;
	
	@Override
	public ResponseFrame saveOrUpdate(PrjApiTest prjApiTest) {
		ResponseFrame frame = new ResponseFrame();
		if(prjApiTest.getId() == null) {
			prjApiTestDao.save(prjApiTest);
		} else {
			prjApiTestDao.update(prjApiTest);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public PrjApiTest get(Integer id) {
		return prjApiTestDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(PrjApiTest prjApiTest) {
		prjApiTest.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = prjApiTestDao.findPrjApiTestCount(prjApiTest);
		List<PrjApiTest> data = null;
		if(total > 0) {
			data = prjApiTestDao.findPrjApiTest(prjApiTest);
		}
		Page<PrjApiTest> page = new Page<PrjApiTest>(prjApiTest.getPage(), prjApiTest.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(Integer id) {
		ResponseFrame frame = new ResponseFrame();
		prjApiTestDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame test(final Integer id) {
		ResponseFrame frame = new ResponseFrame();
		prjApiTestDao.updateTestResult(id, "测试中");
		prjApiTestDtlService.updateStatusByPatId(id, PrjApiTestDtlStatus.WAIT.getCode());
		threadPoolTaskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				PrjApiTest pat = get(id);
				List<PrjApiTestDtl> patds = prjApiTestDtlService.findByPatId(id);
				int sum = patds.size();
				int succNum = 0;
				int failNum = 0;
				for (PrjApiTestDtl patd : patds) {
					ResponseFrame patdFrame = testPatd(pat, patd);
					Integer status = null;
					String testResult = null;
					if(ResponseCode.SUCC.getCode() == patdFrame.getCode().intValue()) {
						//标记状态为成功
						status = PrjApiTestDtlStatus.SUCC.getCode();
						succNum ++;
					} else {
						//标记状态为失败
						status = PrjApiTestDtlStatus.FAIL.getCode();
						failNum ++;
					}
					testResult = patdFrame.getMessage();
					prjApiTestDtlService.updateStatus(patd.getPatId(), patd.getPath(), status, testResult);
				}
				prjApiTestDao.updateTestResult(id, "测试成功[共执行案例: "+sum+"个 - 成功: "+succNum+" - 失败: "+failNum+"]");
			}

			private ResponseFrame testPatd(PrjApiTest pat, PrjApiTestDtl patd) {
				ResponseFrame frame = new ResponseFrame();
				PrjMonitor monitor = prjMonitorService.getService(pat.getPrjId());
				if(monitor == null) {
					frame.setCode(-3);
					frame.setMessage("服务没有启动");
					return frame;
				}
				Map<String, Object> paramsMap = FrameJsonUtil.toMap(patd.getParams());
				//Map<String, Object> paramsMap = new HashMap<String, Object>();
				String url = "http://" + monitor.getRemark() + patd.getPath();
				String clientId = FrameMapUtil.getString(paramsMap, "clientId");
				String time = String.valueOf(System.currentTimeMillis());
				String sercret = FrameMapUtil.getString(paramsMap, "token");
				paramsMap.put("clientId", clientId);
				paramsMap.put("time", time);
				paramsMap.put("sign", AuthUtil.auth(clientId, time, sercret));
				if(FrameStringUtil.isNotEmpty(FrameMapUtil.getString(paramsMap, "token"))) {
					//移除token
					paramsMap.remove("token");
				}
				try {
					String result = FrameHttpUtil.post(url, paramsMap);
					frame = FrameJsonUtil.toObject(result, ResponseFrame.class);
				} catch (IOException e) {
					frame.setCode(-1);
					frame.setMessage("请求接口异常: " + e.getMessage());
				}
				return frame;
			}
		});
		
		frame.setMessage("任务提交成功，具体结果等一段时间后来查看。");
		frame.setSucc();
		return frame;
	}
}
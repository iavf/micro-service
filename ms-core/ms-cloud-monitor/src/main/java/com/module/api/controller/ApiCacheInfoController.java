package com.module.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.module.admin.BaseController;
import com.module.admin.cache.utils.CacheUtil;
import com.module.admin.prj.pojo.PrjInfo;
import com.module.admin.prj.service.PrjInfoService;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 项目cache的接口
 * @author 岳静
 * @date 2016年3月4日 下午6:22:39 
 * @version V1.0
 */
@RestController
public class ApiCacheInfoController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiCacheInfoController.class);
	
	@Autowired
	private PrjInfoService prjInfoService;

	@RequestMapping(name = "批量删除项目的缓存信息", value = "/api/cacheInfo/deleteBatch")
	@ResponseBody
	public ResponseFrame saveBatch(HttpServletRequest request, HttpServletResponse response,
			Integer prjId, String key) {
		ResponseFrame frame = null;
		try {
			PrjInfo prjInfo = prjInfoService.get(prjId);
			if(prjInfo == null) {
				frame = new ResponseFrame();
				frame.setCode(-2);
				frame.setMessage("不存在的项目");
				return frame;
			}
			String serviceId = prjInfo.getCode();
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("key", key);
			frame = CacheUtil.post(serviceId, "/cache/deleteBatch", paramsMap);
		} catch (Exception e) {
			LOGGER.error("批量删除项目的缓存信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		return frame;
	}

}
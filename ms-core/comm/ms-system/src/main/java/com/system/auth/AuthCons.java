package com.system.auth;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.system.auth.model.AuthClient;
import com.system.comm.utils.FrameStringUtil;

/**
 * SSO
 * @author  yuejing
 * @date    2015年11月18日 下午3:09:29 
 * @version 1.0.0
 */
public class AuthCons {

	private final static Map<String, AuthClient> CLIENT_MAP = new ConcurrentHashMap<String, AuthClient>();

	/**
	 * 根据ID获取对象
	 * @param sercret
	 * @return
	 */
	public static AuthClient getId(String id) {
		if (FrameStringUtil.isEmpty(id)) {
			return null;
		}
		return CLIENT_MAP.get(id);
	}

	/**
	 * 获取第一项的内容
	 * @return
	 */
	public static AuthClient getFirst() {
		if(CLIENT_MAP.size() > 0) {
			Entry<String, AuthClient> entry = CLIENT_MAP.entrySet().iterator().next();
			return entry.getValue();
			/*Iterator<Entry<String, AuthClient>> entryKeyIterator = clientMap.entrySet().iterator();
			while (entryKeyIterator.hasNext()) {
				Entry<String, AuthClient> e = entryKeyIterator.next();
				return e.getValue();
			}*/
		}
		return null;
	}

	public static synchronized void put(String id, AuthClient client) {
		CLIENT_MAP.put(id, client);
	}
}
package com.ms.task.config;


import java.io.IOException;
import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

/**
 * 定时任务的配置
 * @author yuejing
 * @date 2017年5月26日 上午9:44:52
 */
@Configuration
public class QuartzScheduleConfig {

    /*@Autowired
    private Environment env;*/

    @Bean
    @Lazy(false)
    public SchedulerFactoryBean schedulerFactoryBean() throws IOException {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setOverwriteExistingJobs(true);
        // 延时启动
        factory.setStartupDelay(20);
        Properties quartz = new Properties();
        // 线程池线程数
        quartz.setProperty("org.quartz.threadPool.threadCount", "100");
		factory.setQuartzProperties(quartz);
        // 自定义Job Factory，用于Spring注入
        //factory.setJobFactory(myJobFactory);
        return factory;
    }
}